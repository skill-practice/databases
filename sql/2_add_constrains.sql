-- create indexes
ALTER TABLE students
    ADD CONSTRAINT fk_students__primary_skill
        FOREIGN KEY (primary_skill)
            REFERENCES subjects (id);

ALTER TABLE exam_results
    ADD CONSTRAINT fk_exam_results__subject_id
        FOREIGN KEY (subject_id) REFERENCES subjects (id),
    ADD CONSTRAINT fk_exam_results__student_id
        FOREIGN KEY (student_id) REFERENCES students (id);

CREATE INDEX idx_students__primary_skill ON students (primary_skill);
CREATE INDEX idx_exam_results__student_id ON exam_results (student_id);
CREATE INDEX idx_exam_results__subject_id ON exam_results (subject_id);
