INSERT INTO subjects(subject_name, tutor)
VALUES ('Mathematics', 'Jane Austin'),
       ('Physics', 'Ben Stifler'),
       ('Biology', 'Jack McKensey'),
       ('History', 'Jason Henser'),
       ('English', 'Benedict Catcher');

INSERT INTO students(username, name, surname, date_of_birth, phone_numbers, primary_skill, created_datetime)
VALUES ('m_mathers', 'Marshal', 'Mathers', '1994-10-17'::DATE, '123456789, 147852369',
        (SELECT id FROM subjects WHERE subject_name = 'English'), '2021-08-25 12:14:32.000z'::TIMESTAMP),
       ('i_wells', 'Immanuel', 'Wells', '1994-10-18'::DATE, '302-583-9009',
        (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), '2021-10-13 12:17:15.000z'::TIMESTAMP),
       ('p_fischer', 'Parker', 'Fischer', '1994-10-19'::DATE, '630-749-3053',
        (SELECT id FROM subjects WHERE subject_name = 'Physics'), '2021-10-14 12:17:15.000z'::TIMESTAMP),
       ('g_mayo', 'Guadalupe', 'Mayo', '1994-10-20'::DATE, '470-355-4992',
        (SELECT id FROM subjects WHERE subject_name = 'Biology'), '2021-10-15 12:17:15.000z'::TIMESTAMP),
       ('e_petersen', 'Evelyn', 'Petersen', '1994-10-21'::DATE, '215-866-4377',
        (SELECT id FROM subjects WHERE subject_name = 'History'), '2021-10-16 12:17:15.000z'::TIMESTAMP),
       ('a_rios', 'Arturo', 'Rios', '1994-10-22'::DATE, '401-276-7503',
        (SELECT id FROM subjects WHERE subject_name = 'English'), '2021-10-17 12:17:15.000z'::TIMESTAMP),
       ('k_james', 'Kenna', 'James', '1994-10-23'::DATE, '251-544-6785',
        (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), '2021-10-18 12:17:15.000z'::TIMESTAMP),
       ('a_mays', 'Alexzander', 'Mays', '1994-10-24'::DATE, '775-251-1396',
        (SELECT id FROM subjects WHERE subject_name = 'Physics'), '2021-10-19 12:17:15.000z'::TIMESTAMP),
       ('m_camacho', 'Monique', 'Camacho', '1994-10-25'::DATE, '423-875-6536',
        (SELECT id FROM subjects WHERE subject_name = 'Biology'), '2021-10-20 12:17:15.000z'::TIMESTAMP),
       ('j_zuniga', 'Jordin', 'Zuniga', '1994-10-26'::DATE, '541-560-6459',
        (SELECT id FROM subjects WHERE subject_name = 'History'), '2021-10-21 12:17:15.000z'::TIMESTAMP),
       ('k_cuevas', 'Kali', 'Cuevas', '1994-10-27'::DATE, '325-258-5129',
        (SELECT id FROM subjects WHERE subject_name = 'English'), '2021-10-22 12:17:15.000z'::TIMESTAMP),
       ('z_solomon', 'Zoey', 'Solomon', '1994-10-28'::DATE, '808-535-1215',
        (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), '2021-10-23 12:17:15.000z'::TIMESTAMP),
       ('j_cross', 'Jessica', 'Cross', '1994-10-29'::DATE, '317-929-6924',
        (SELECT id FROM subjects WHERE subject_name = 'Physics'), '2021-10-24 12:17:15.000z'::TIMESTAMP);

INSERT INTO exam_results(student_id, subject_id, mark)
VALUES ((SELECT id FROM students WHERE username = 'm_mathers'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 2),
       ((SELECT id FROM students WHERE username = 'm_mathers'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 3),
       ((SELECT id FROM students WHERE username = 'm_mathers'), (SELECT id FROM subjects WHERE subject_name = 'English'), 10),
       ((SELECT id FROM students WHERE username = 'i_wells'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 9),
       ((SELECT id FROM students WHERE username = 'i_wells'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 6),
       ((SELECT id FROM students WHERE username = 'i_wells'), (SELECT id FROM subjects WHERE subject_name = 'English'), 3),
       ((SELECT id FROM students WHERE username = 'i_wells'), (SELECT id FROM subjects WHERE subject_name = 'History'), 10),
       ((SELECT id FROM students WHERE username = 'p_fischer'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 7),
       ((SELECT id FROM students WHERE username = 'p_fischer'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 9),
       ((SELECT id FROM students WHERE username = 'p_fischer'), (SELECT id FROM subjects WHERE subject_name = 'English'), 6),
       ((SELECT id FROM students WHERE username = 'p_fischer'), (SELECT id FROM subjects WHERE subject_name = 'History'), 5),
       ((SELECT id FROM students WHERE username = 'p_fischer'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 8),
       ((SELECT id FROM students WHERE username = 'g_mayo'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 9),
       ((SELECT id FROM students WHERE username = 'g_mayo'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 7),
       ((SELECT id FROM students WHERE username = 'g_mayo'), (SELECT id FROM subjects WHERE subject_name = 'English'), 8),
       ((SELECT id FROM students WHERE username = 'g_mayo'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 10),
       ((SELECT id FROM students WHERE username = 'e_petersen'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 9),
       ((SELECT id FROM students WHERE username = 'e_petersen'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 7),
       ((SELECT id FROM students WHERE username = 'e_petersen'), (SELECT id FROM subjects WHERE subject_name = 'English'), 2),
       ((SELECT id FROM students WHERE username = 'e_petersen'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 3),
       ((SELECT id FROM students WHERE username = 'a_rios'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 9),
       ((SELECT id FROM students WHERE username = 'a_rios'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 7),
       ((SELECT id FROM students WHERE username = 'a_rios'), (SELECT id FROM subjects WHERE subject_name = 'English'), 9),
       ((SELECT id FROM students WHERE username = 'a_rios'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 10),
       ((SELECT id FROM students WHERE username = 'k_james'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 9),
       ((SELECT id FROM students WHERE username = 'k_james'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 7),
       ((SELECT id FROM students WHERE username = 'k_james'), (SELECT id FROM subjects WHERE subject_name = 'English'), 9),
       ((SELECT id FROM students WHERE username = 'k_james'), (SELECT id FROM subjects WHERE subject_name = 'History'), 3),
       ((SELECT id FROM students WHERE username = 'k_james'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 1),
       ((SELECT id FROM students WHERE username = 'a_mays'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 3),
       ((SELECT id FROM students WHERE username = 'a_mays'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 10),
       ((SELECT id FROM students WHERE username = 'a_mays'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 1),
       ((SELECT id FROM students WHERE username = 'm_camacho'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 3),
       ((SELECT id FROM students WHERE username = 'm_camacho'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 4),
       ((SELECT id FROM students WHERE username = 'm_camacho'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 8),
       ((SELECT id FROM students WHERE username = 'j_zuniga'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 3),
       ((SELECT id FROM students WHERE username = 'j_zuniga'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 1),
       ((SELECT id FROM students WHERE username = 'j_zuniga'), (SELECT id FROM subjects WHERE subject_name = 'English'), 8),
       ((SELECT id FROM students WHERE username = 'j_zuniga'), (SELECT id FROM subjects WHERE subject_name = 'History'), 10),
       ((SELECT id FROM students WHERE username = 'j_zuniga'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 4),
       ((SELECT id FROM students WHERE username = 'k_cuevas'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 3),
       ((SELECT id FROM students WHERE username = 'k_cuevas'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 2),
       ((SELECT id FROM students WHERE username = 'k_cuevas'), (SELECT id FROM subjects WHERE subject_name = 'English'), 8),
       ((SELECT id FROM students WHERE username = 'k_cuevas'), (SELECT id FROM subjects WHERE subject_name = 'History'), 7),
       ((SELECT id FROM students WHERE username = 'k_cuevas'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 5),
       ((SELECT id FROM students WHERE username = 'z_solomon'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 2),
       ((SELECT id FROM students WHERE username = 'z_solomon'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 3),
       ((SELECT id FROM students WHERE username = 'z_solomon'), (SELECT id FROM subjects WHERE subject_name = 'English'), 10),
       ((SELECT id FROM students WHERE username = 'j_cross'), (SELECT id FROM subjects WHERE subject_name = 'Mathematics'), 7),
       ((SELECT id FROM students WHERE username = 'j_cross'), (SELECT id FROM subjects WHERE subject_name = 'Physics'), 9),
       ((SELECT id FROM students WHERE username = 'j_cross'), (SELECT id FROM subjects WHERE subject_name = 'English'), 6),
       ((SELECT id FROM students WHERE username = 'j_cross'), (SELECT id FROM subjects WHERE subject_name = 'History'), 5),
       ((SELECT id FROM students WHERE username = 'j_cross'), (SELECT id FROM subjects WHERE subject_name = 'Biology'), 8);
