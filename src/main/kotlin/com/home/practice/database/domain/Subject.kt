package com.home.practice.database.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "subjects")
class Subject(

        @Column(name = "subject_name")
        val name: String,

        @Column(name = "tutor")
        val tutor: String

) : BaseAuditedEntity()