package com.home.practice.database.domain

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "students")
class Student(
        @Column(name = "username")
        val username: String,

        @Column(name = "name")

        val name: String,
        @Column(name = "surname")

        val surname: String?,

        @Column(name = "date_of_birth")
        val dateOfBirth: LocalDate,

        @Column(name = "phone_numbers")
        val phoneNumbers: String,

        @ManyToOne
        @JoinColumn(name = "primary_skill")
        val primarySkill: Subject

) : BaseAuditedEntity()