package com.home.practice.database.domain

import javax.persistence.*

@Entity
@Table(name = "exam_results")
class ExamResult(

        @ManyToOne
        @JoinColumn(name = "student_id")
        val student: Student,

        @ManyToOne
        @JoinColumn(name = "subject_id")
        val subject: Subject,

        @Column(name = "mark")
        val mark: Int

) : BaseEntity()