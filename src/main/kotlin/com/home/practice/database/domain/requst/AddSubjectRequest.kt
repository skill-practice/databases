package com.home.practice.database.domain.requst

data class AddSubjectRequest(
        val name: String,
        val tutor: String
)
