package com.home.practice.database.domain.requst

import com.home.practice.database.domain.Student
import com.home.practice.database.domain.Subject

data class AddExamResultRequest(
        val student: Student,
        val subject: Subject,
        val mark: Int
)
