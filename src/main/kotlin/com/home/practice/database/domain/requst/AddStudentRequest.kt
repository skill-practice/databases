package com.home.practice.database.domain.requst

import com.home.practice.database.domain.Subject
import java.time.LocalDate

data class AddStudentRequest(
        val username: String,
        val name: String,
        val surname: String?,
        val dateOfBirth: LocalDate,
        val phoneNumbers: String,
        val primarySkill: Subject,
)
