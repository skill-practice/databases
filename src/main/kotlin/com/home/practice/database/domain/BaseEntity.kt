package com.home.practice.database.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@MappedSuperclass
abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Transient
    val className: String = this.javaClass.simpleName

    @JsonIgnore
    fun isPersisted(): Boolean {
        val tempId = id
        return null != tempId && 0 < tempId
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseEntity

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result *= 31
        return result
    }
}

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class BaseAuditedEntity : BaseEntity(), Serializable {

    @Column(name = "created_datetime")
    var createdAt: LocalDateTime? = null

    @Column(name = "updated_datetime")
    var updatedAt: LocalDateTime? = null

}
