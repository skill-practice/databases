package com.home.practice.database.service

import com.home.practice.database.domain.ExamResult
import com.home.practice.database.domain.Student
import com.home.practice.database.domain.Subject
import com.home.practice.database.domain.requst.AddExamResultRequest
import com.home.practice.database.repository.ExamResultRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface ExamResultService {
    fun add(request: AddExamResultRequest): ExamResult
    fun findAllByStudent(student: Student): Collection<ExamResult>
    fun findAllBySubject(subject: Subject): Collection<ExamResult>
}

@Service
internal class ExamResultServiceImpl(
        private val repository: ExamResultRepository
) : ExamResultService {

    @Transactional
    override fun add(request: AddExamResultRequest): ExamResult {
        val result = ExamResult(
                subject = request.subject,
                student = request.student,
                mark = request.mark
        )

        return repository.save(result)
    }

    @Transactional(readOnly = true)
    override fun findAllByStudent(student: Student): Collection<ExamResult> {
        return repository.findByStudent(student)
    }

    @Transactional(readOnly = true)
    override fun findAllBySubject(subject: Subject): Collection<ExamResult> {
        return repository.findBySubject(subject)
    }

}
