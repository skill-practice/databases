package com.home.practice.database.service

import com.home.practice.database.domain.Student
import com.home.practice.database.domain.requst.AddStudentRequest
import com.home.practice.database.repository.StudentRepository
import javassist.NotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional

interface StudentService {
    fun add(request: AddStudentRequest): Student
    fun addReadUncommitted(request: AddStudentRequest): Student
    fun findByUsername(username: String): Student?
    fun findByUsernameReadCommitted(username: String): Student?
    fun getByUsername(username: String): Student
}

@Service
internal class StudentServiceImpl(
        private val repository: StudentRepository
) : StudentService {

    @Transactional(isolation = Isolation.SERIALIZABLE)
    override fun add(request: AddStudentRequest): Student {
        return saveDefault(request)
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    override fun addReadUncommitted(request: AddStudentRequest): Student {
        return saveDefault(request)
    }

    @Transactional
    override fun findByUsername(username: String): Student? {
        return repository.findByUsername(username)
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    override fun findByUsernameReadCommitted(username: String): Student? {
        return repository.findByUsername(username)
    }

    @Transactional
    override fun getByUsername(username: String): Student {
        return findByUsername(username)
                ?: throw NotFoundException("Student by username '$username' not found")
    }

    private fun saveDefault(request: AddStudentRequest): Student {
        val student = Student(
                username = request.username,
                name = request.name,
                surname = request.surname,
                dateOfBirth = request.dateOfBirth,
                phoneNumbers = request.phoneNumbers,
                primarySkill = request.primarySkill,
        )
        return repository.save(student)
    }
}
