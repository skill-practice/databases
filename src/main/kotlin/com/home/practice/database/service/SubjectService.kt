package com.home.practice.database.service

import com.home.practice.database.domain.Subject
import com.home.practice.database.domain.requst.AddSubjectRequest
import com.home.practice.database.repository.SubjectRepository
import org.springframework.stereotype.Service

interface SubjectService {
    fun add(request: AddSubjectRequest): Subject
}

@Service
internal class SubjectServiceImpl(
        private val repository: SubjectRepository
) : SubjectService {

    override fun add(request: AddSubjectRequest): Subject {
        val subject = Subject(
                name = request.name,
                tutor = request.tutor
        )

        return repository.save(subject)
    }

}
