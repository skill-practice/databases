package com.home.practice.database

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DatabaseApp

fun main() {
    runApplication<DatabaseApp>()
}