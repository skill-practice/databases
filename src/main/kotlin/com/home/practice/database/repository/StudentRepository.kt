package com.home.practice.database.repository

import com.home.practice.database.domain.Student

interface StudentRepository : BaseRepository<Student> {
    fun findByUsername(username: String): Student?
}
