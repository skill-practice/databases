package com.home.practice.database.repository

import com.home.practice.database.domain.Subject

interface SubjectRepository: BaseRepository<Subject>
