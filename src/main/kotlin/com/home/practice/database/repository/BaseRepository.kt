package com.home.practice.database.repository

import com.home.practice.database.domain.BaseEntity
import org.springframework.data.jpa.repository.JpaRepository

interface BaseRepository<T : BaseEntity> : JpaRepository<T, Long>
