package com.home.practice.database.repository

import com.home.practice.database.domain.ExamResult
import com.home.practice.database.domain.Student
import com.home.practice.database.domain.Subject

interface ExamResultRepository : BaseRepository<ExamResult> {
    fun findByStudent(student: Student): Collection<ExamResult>
    fun findBySubject(subject: Subject): Collection<ExamResult>
}
