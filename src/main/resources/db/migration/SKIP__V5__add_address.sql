CREATE TABLE student_address
(
    id            BIGSERIAL PRIMARY KEY,
    address_line1 VARCHAR(256) NOT NULL,
    address_line2 VARCHAR(256),
    city          VARCHAR(256)
);

CREATE TABLE revisions
(
    id BIGSERIAL PRIMARY KEY
);

CREATE TABLE student_address_aud
(
    id            BIGINT,
    address_line1 VARCHAR(256),
    address_line2 VARCHAR(256),
    city          VARCHAR(256),
    rev_id        BIGINT
);

CREATE OR REPLACE FUNCTION create_updated_address()
    RETURNS TRIGGER as
$$
DECLARE
    revisionId BIGINT;
BEGIN
    INSERT INTO revisions DEFAULT VALUES RETURNING id INTO revisionId;

    INSERT INTO student_address_aud(id, address_line1, address_line2, city, rev_id)
    VALUES (new.id, new.address_line1, new.address_line2, new.city, revisionId);
    new.address_line1 = old.address_line1;
    new.address_line1 = old.address_line2;
    new.city = old.city;
    RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tg_address_update
    AFTER UPDATE
    ON student_address
    FOR EACH ROW
EXECUTE PROCEDURE create_updated_address();

INSERT INTO student_address(address_line1, address_line2, city)
VALUES ('a1','a2','c1');

UPDATE student_address
SET address_line2 = 'a+2'
WHERE id = 1;
