-- create table
CREATE TABLE subjects
(
    id           BIGSERIAL PRIMARY KEY,
    subject_name VARCHAR(128) NOT NULL,
    tutor        VARCHAR(128) NOT NULL
);

CREATE TABLE students
(
    id               BIGSERIAL PRIMARY KEY,
    username         VARCHAR(64) NOT NULL UNIQUE ,
    name             VARCHAR(64) NOT NULL,
    surname          VARCHAR(64),
    date_of_birth    DATE        NOT NULL,
    phone_numbers    VARCHAR(128),
    primary_skill    BIGINT,
    created_datetime TIMESTAMP,
    updated_datetime TIMESTAMP
);

CREATE TABLE exam_results
(
    id         BIGSERIAL PRIMARY KEY,
    student_id BIGINT NOT NULL,
    subject_id BIGINT NOT NULL,
    mark       INTEGER
);

