-- a. Find user by name (exact match)
SELECT *
FROM students
WHERE name = 'Parker';

-- b. Find user by surname (partial match)
SELECT *
FROM students
WHERE surname ILIKE '%sch%';

-- c. Find user by phone number (partial match)
SELECT *
FROM students
WHERE students.phone_numbers ILIKE '%86%';

-- d. Find user with marks by user surname (partial match)
SELECT s.id, s.username, s.name, s.surname
FROM students s
         JOIN exam_results er on s.id = er.student_id
WHERE er.mark = 5
GROUP BY s.id, s.username, s.name, s.surname;

-- 5. Add trigger that will update column updated_datetime to current date in case of updating any of student. (0.3 point)
CREATE OR REPLACE FUNCTION students_update_timestamp()
    RETURNS TRIGGER as
$$
BEGIN
    RAISE NOTICE 'NEW :%', new;
    new.updated_datetime := now();
    RETURN new;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_students_update_timestamp
    BEFORE UPDATE
    ON students
    FOR EACH ROW
EXECUTE PROCEDURE students_update_timestamp();


-- 6 Add validation on DB level that will check username on special characters (reject student name with next characters '@', '#', '$'). (0.3 point)
ALTER TABLE students
    ADD CHECK ( username NOT LIKE ALL ('%@%', '%#%', '%$%'));

-- 7 Create snapshot that will contain next data: student name, student surname, subject name,
-- mark (snapshot means that in case of changing some data in source table – your snapshot should not change). (0.3 point)


-- 8 Create function that will return average mark for input user. (0.3 point)
CREATE OR REPLACE FUNCTION get_average_mark_for_student(studentId BIGINT)
    RETURNS INTEGER AS
$$
BEGIN
    SELECT avg(es.mark) FROM exam_results es WHERE es.student_id = studentId;
END
$$;


-- 9. Create function that will return avarage mark for input subject name. (0.3 point).
CREATE OR REPLACE FUNCTION get_average_mark_for_subject(subjectId BIGINT)
    RETURNS INTEGER AS
$$
BEGIN
    SELECT avg(es.mark) FROM exam_results es WHERE es.subject_id = subjectId;
END
$$;


-- 10. Create function that will return student at "red zone" (red zone means at least 2 marks <=3). (0.3 point)
CREATE OR REPLACE FUNCTION find_all_red_zone()
    RETURNS INTEGER AS
$$
BEGIN
    SELECT s.username, s.name, s.surname
    FROM exam_results es
             JOIN students s on es.student_id = s.id
    WHERE es.mark <= 3
    GROUP BY s.name, s.username, s.surname
    HAVING count(*) >= 2;
END;
$$;


