package com.home.practice.database.service

import com.home.practice.database.data.DomainStudentData
import com.home.practice.database.data.DomainSubjectData
import com.home.practice.database.domain.Student
import com.home.practice.database.domain.Subject
import com.home.practice.database.domain.requst.AddStudentRequest
import com.home.practice.database.repository.StudentRepository
import com.home.practice.database.repository.SubjectRepository
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional

class StudentServiceTest(
) : BaseServiceTest() {

    @Autowired
    lateinit var studentRepository: StudentRepository
    @Autowired
    lateinit var studentService: StudentService
    @Autowired
    lateinit var subjectRepository: SubjectRepository

    @AfterEach
    fun tearDown() {
        studentRepository.deleteAll()
        subjectRepository.deleteAll()
    }

    @Test
    fun testReadCommitted() {
        val username = "username1"

        val subject: Subject = DomainSubjectData.getSubject()
        subjectRepository.save(subject)


        val addStudentRequest: AddStudentRequest = DomainStudentData.getAddStudentRequest(
                username = username,
                primarySkill = subject
        )
        val savedStudent: Student = studentService.add(addStudentRequest)
//        val student: Student = DomainStudentData.getStudent(
//                username = username,
//                primarySkill = subject
//        )
//        studentRepository.save(student)

        val foundStudent: Student? = studentService.findByUsernameReadCommitted(username)

        assertNotNull(foundStudent)
        assertEquals(savedStudent.id, foundStudent?.id)
    }

}