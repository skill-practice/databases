package com.home.practice.database.data

import com.home.practice.database.domain.Student
import com.home.practice.database.domain.Subject
import com.home.practice.database.domain.requst.AddStudentRequest
import java.time.LocalDate

object DomainStudentData {

    fun getAddStudentRequest(
            username: String = "username",
            name: String = "name",
            surname: String? = "surname",
            dateOfBirth: LocalDate = LocalDate.of(2021, 11, 4),
            phoneNumbers: String = "phoneNumbers",
            primarySkill: Subject = DomainSubjectData.getSubject(),
    ): AddStudentRequest {
        return AddStudentRequest(
                username = username,
                name = name,
                surname = surname,
                dateOfBirth = dateOfBirth,
                phoneNumbers = phoneNumbers,
                primarySkill = primarySkill,
        )
    }

    fun getStudent(
            username: String = "username",
            name: String = "name",
            surname: String? = "surname",
            dateOfBirth: LocalDate = LocalDate.of(2021, 11, 4),
            phoneNumbers: String = "phoneNumbers",
            primarySkill: Subject = DomainSubjectData.getSubject(),
    ): Student {
        return Student(
                username = username,
                name = name,
                surname = surname,
                dateOfBirth = dateOfBirth,
                phoneNumbers = phoneNumbers,
                primarySkill = primarySkill,
        )
    }
}