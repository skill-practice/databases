package com.home.practice.database.data

import com.home.practice.database.domain.Subject
import com.home.practice.database.domain.requst.AddSubjectRequest

object DomainSubjectData {

    fun getAddSubjectRequest(
            name: String = "subject name",
            tutor: String = "tutor"
    ): AddSubjectRequest {
        return AddSubjectRequest(
                name = name,
                tutor = tutor,
        )
    }

    fun getSubject(
            name: String = "subject name",
            tutor: String = "tutor"
    ): Subject {
        return Subject(
                name = name,
                tutor = tutor,
        )
    }
}